section .text
%include "lib.inc"
global find_word
global print_dict

;в rdi- нужный ключ. в rsi - указатель на первую пару
find_word:
    .loop:
    	push rsi
    	add rsi, 8	; Берем следующий элемент, сравниваем значение 
    	call string_equals ; с строкой ключа
    	pop rsi
    	cmp rax, 0 ; Проверка на соответствие
    	jne .found 
    	mov rsi, [rsi] 	; Переходим к следующему
    	cmp rsi, 0 	; если элемента нет,
    	je .not_found ; то завершаем поиск
    	jmp .loop 
    .found:
        mov rax, rsi ; rax = адрес начала вхождения 
        ret
    .not_found:
        xor rax, rax
        ret	

       
print_dict:

    .loop:
        push rsi            
        push rdi            
        add rdi, 8          ; получаем указатель ключа
        call print_string  ; вызываем
        call print_newline
        pop rdi            
        pop rsi            
        mov rdi, [rdi]      ; указатель следующего элемента
        test rdi, rdi
        jnz .loop           ; повтор цикла, если не достигнут конец словаря
        xor rax, rax        ; возврат 0
    ret   